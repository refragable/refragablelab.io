[![pipeline status](https://gitlab.com/refragable/refragable.gitlab.io/badges/master/pipeline.svg)](https://gitlab.com/refragable/refragable.gitlab.io/-/commits/master) 
[![coverage report](https://gitlab.com/refragable/refragable.gitlab.io/badges/master/coverage.svg)](https://gitlab.com/refragable/refragable.gitlab.io/-/commits/master)

[Personal blog.](https://refragable.gitlab.io/)

## License

All blog content is [CC-BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)

All blog code is [GPLv3 or later](https://www.gnu.org/copyleft/gpl.html)

## Credits

[Hugo](https://gohugo.io/) static site generator  
[m10c](https://github.com/vaga/hugo-theme-m10c) theme  
[Gitlab Pages](https://docs.gitlab.com/ce/user/project/pages/) hosting  