---
title: "Nitterbot Published"
date: 2020-07-25T07:26:32-07:00
draft: false
---

Working with @overheadscallop:matrix.org, I've developed Nitterbot ([gitlab](https://gitlab.com/refragable/matrix-nitterbot), [github](https://github.com/refragable/matrix-nitterbot)) for use on the [Security Twitter Feeds channel](https://freshoxygen.neocities.org/) based on Brendan Abolivier's [Matrix Tweetalong bot](https://github.com/babolivier/matrix-tweetalong-bot).

Feel free to open issues or to submit pull requests!