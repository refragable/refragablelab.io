---
title: "Weechat GnuTLS error on Fedora 32"
date: 2020-07-19T16:11:10-07:00
draft: false
---

This post is to help those who may be having problems with weechat on Fedora in the future.

## The problem

After switching distros from Ubuntu 18.04 to Fedora 32, I migrated my `$HOME/.weechat` directory and started weechat in a tmux session. As you can see in the log below, gnutls was not able to verify LetsEncrypt TLS certificates!

```
│14:29:30   freenode  -- | irc: reconnecting to server...
│14:29:30   freenode  -- | irc: connecting to server chat.freenode.net/6697 (SSL)...
│14:29:31   freenode  -- | gnutls: connected using 2048-bit Diffie-Hellman shared secret exchange
│14:29:31   freenode  -- | gnutls: receiving 2 certificates
│14:29:31   freenode  -- |  - certificate[1] info:
│14:29:31   freenode  -- |    - subject `CN=tepper.freenode.net', issuer `CN=Let's Encrypt Authority X3,O=Let's Encrypt,C=US', serial
│                        | 0x036c5fabfbf1d540b33ad5d1e94826fbca9e, RSA key 4096 bits, signed using RSA-SHA256, activated `2020-06-29
│                        | 04:49:50 UTC', expires `2020-09-27 04:49:50 UTC',
│                        | pin-sha256="WjH6UueCkVey1W3W1fEhEoAfwFq11WFFPDEuEJ0cupE="
│14:29:31   freenode  -- |  - certificate[2] info:
│14:29:31   freenode  -- |    - subject `CN=Let's Encrypt Authority X3,O=Let's Encrypt,C=US', issuer `CN=DST Root CA X3,O=Digital
│                        | Signature Trust Co.', serial 0x0a0141420000015385736a0b85eca708, RSA key 2048 bits, signed using
│                        | RSA-SHA256, activated `2016-03-17 16:40:46 UTC', expires `2021-03-17 16:40:46 UTC',
│                        | pin-sha256="YLh1dUR9y6Kja30RrAn7JKnbQG/uEtLMkBgFF2Fuihg="
│14:29:31   freenode =!= | gnutls: peer's certificate is NOT trusted
│14:29:31   freenode =!= | gnutls: peer's certificate issuer is unknown
│14:29:31   freenode =!= | irc: TLS handshake failed
|14:29:31   freenode =!= | irc: error: Error in the certificate.
│14:29:31   freenode  -- | irc: reconnecting to server in 1 minute, 20 seconds
```

## Troubleshooting

A search on the [weechat bugtracker](https://github.com/weechat/weechat/issues?q=gnutls+unknown) had some useful tips. The suggestions were to set `ssl_verify` to `off` (which is insecure) or to find the correct location of the `gnutls_ca_file`. I opted to do the latter.

weechat's default value for `weechat.network.gnutls_ca_file` is `/etc/pki/tls/certs/ca-bundle.crt`

As we can see in the below, the default file is symlinked to `/etc/pki/ca-trust/extracted/pem/tls-ca-bundle.pem` in Fedora:

```
[me@pc /etc/pki/tls/certs]$ ls -la
total 8
drwxr-xr-x. 2 root root 4096 Jul  7 09:18 .
drwxr-xr-x. 5 root root 4096 Jul  7 09:18 ..
lrwxrwxrwx. 1 root root   49 Jun 16 16:00 ca-bundle.crt -> /etc/pki/ca-trust/extracted/pem/tls-ca-bundle.pem
lrwxrwxrwx. 1 root root   55 Jun 16 16:00 ca-bundle.trust.crt -> /etc/pki/ca-trust/extracted/openssl/ca-bundle.trust.crt
```

## The solution

Therefore, we can run `/set weechat.network.gnutls_ca_file "/etc/pki/ca-trust/extracted/pem/tls-ca-bundle.pem"` to set the correct file location.

Don't forget to do `/save` to preserve your configuration changes!
